/**
 * Task.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	name:{
  		type:'string',
  		required:'true'
  	},
  	description:{
  		type:'string',
  	},
  	time:{
  		type:'integer',
  	},
  	due_date:{
  		type:'date',
  		required:true
  	},
  	status:{
  		type:'boolean',
  		defaultsTo:false
  	},
  	user:{
  		collection:'user',
  		via: 'id'
  	}

  }
};

